<?php

/**
 * Arbel core app
 */

namespace Arbel\Admin;

use Zend\ServiceManager\ServiceManager;
use Arbel\ObjectManager;
use Arbel\Base\ModelAbstract;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Zend\Session\Validator;
use Zend\Db\Adapter\Adapter;
use Zend\EventManager\EventManagerInterface;
use Arbel\Updater\ModuleUpdater;
use Illuminate\Database\Capsule\Manager as Capsule;
use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\StorageInterface;
use Arbel\Google\Translate;
use Arbel\Service\Factory;
use Arbel\Cache\CacheInterface;
use Arbel\Cache;
use Arbel\Cache\CacheManager;
use Zend\Mail\Transport\SmtpOptions;
use Plivo\RestAPI;
use Arbel\Sms\SmsInterface;
use Arbel\Sms\Plivo;

class Module {

    const VERSION = '0.0.1';

    public function getConfig() {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getUpdaterVersion() {
        return $this::VERSION;
    }

    public static function getTablePrefix() {
        return 'admin';
    }

    public function onBootstrap($e) {
        $app = $e->getTarget();
        $services = $app->getServiceManager();
        $this->initDi($services);
        $services->addAbstractFactory($services->get('DiStrictAbstractServiceFactory'));

    }

    public function initDi($sm) {

        $sm->get('Di')->instanceManager()
        ;
    }

}
