<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Arbel\Admin\Controller;

use Zend\View\Model\ViewModel;
use Arbel\Cache\CacheManager;
use Arbel\Controller\AbstractActionController;

/**
 * Cache controller
 */
class MainController extends AbstractActionController
{
    /**
     * CacheManager
     * @var CacheManager
     */
    protected $cacheManager;

    /**
     * Inject objects
     * @param StorageInterface $cache
     */
    public function setInjections(CacheManager $cacheManager)
    {
        $this->cacheManager = $cacheManager;
    }

    public function indexAction()
    {
//        if (!$this->server->verifyResourceRequest(OAuth2Request::createFromGlobals())) {
//    // Not authorized return 401 error
//    $this->getResponse()->setStatusCode(401);
//    return;
//}
    }

}