import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule} from "@angular/router";
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { MatSnackBarModule } from '@angular/material';
import { WebService } from '@resources/core/services/web.service';
import { AuthService } from '@resources/core/services/auth.service';
import { InfoService } from '@resources/core/services/info.service'; 
import { SharedService  } from '@resources/core/services/shared.service'; 
import { AuthGuard } from '@resources/core/guards/auth.g';
import { LoginGuard } from '@resources/core/guards/login.g';
import { PageSharedService } from '@resources/admin/app/services/page.shared.service';
import 'hammerjs';

import {ROUTES} from "./app.routes";
import { AppComponent } from './app.component'; 

// App views
import {DashboardsModule} from "./views/dashboards/dashboards.module";
import {AppviewsModule} from "./views/appviews/appviews.module";
//import {NgxDatatableStubModule} from "./modules/ngxDatatableStub.module";

// App modules/components
import {LayoutsModule} from "./components/common/layouts/layouts.module";

@NgModule({
  declarations: [
    AppComponent 
  ], 
  imports: [
    FormsModule, 
    BrowserModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    HttpModule, 
    DashboardsModule,
    LayoutsModule,
    AppviewsModule,
   // NgxDatatableStubModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    WebService, 
    AuthService, 
    InfoService, 
    SharedService,
    PageSharedService, 
    AuthGuard,
    LoginGuard
  ],
  bootstrap: [AppComponent],
  entryComponents: [AppComponent]
})
export class AdminModule { }
