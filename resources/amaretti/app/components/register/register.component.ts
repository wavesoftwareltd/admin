
import { Component } from '@angular/core';
import {RegisterComponent} from "@resources/core/components/register/register.component";

@Component({
  selector: 'register',
  templateUrl:'register.template.html',
  styleUrls : ['register.style.scss']
})

export class AmarettiRegisterComponent extends RegisterComponent {

 }
      