import { Component } from '@angular/core';
import {LoginComponent} from "@resources/core/components/login/login.component";


@Component({
  selector: 'login',
  templateUrl:'login.template.html',
  styleUrls : ['login.style.scss']
})

export class AmarettiLoginComponent extends LoginComponent {

  login() {
    var that = this;
    this.isSubmiting = true;
       this.auth.login(<string>this.email,<string>this.password,()=>{
         that.isSubmiting = false;
       },()=>{
         that.isSubmiting = false;
       });
   }
 }
      