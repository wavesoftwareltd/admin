import { Component, OnDestroy, OnInit, } from '@angular/core';
import { PageSharedService } from '@resources/admin/app/services/page.shared.service';

@Component({
  selector: 'basic-page',
  templateUrl: 'basic.template.html'
})
export class BasicPageComponent implements OnDestroy, OnInit {

  public header = 'Empty page Header';
  public breadcrumbs = [
    { title: 'Home', link: 'home' },
    { title: 'Pages2', link: 'page' }
  ];

  public constructor(
    protected _sharedService: PageSharedService
  ) { 
   
  }

  public ngOnInit(): any {
    //this.nav.className += " white-bg";
  }


  public ngOnDestroy(): any {
    //this.nav.classList.remove("white-bg");
  }

}
