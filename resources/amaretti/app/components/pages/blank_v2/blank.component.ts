import { Component, OnDestroy, OnInit, } from '@angular/core';

@Component({
  selector: 'blank-page',
  templateUrl: 'blank.template.html'
})
export class BlankPageComponentV2 implements OnDestroy, OnInit  {

  public header = 'Empty page Header';
  public breadcrumbs = [
    {title:'Home',link : 'home'},
    {title :'Pages2',link:'page'} 
  ];

public constructor() {
 // this.nav = document.querySelector('nav.navbar');
}

public ngOnInit():any {
  //this.nav.className += " white-bg";
}


public ngOnDestroy():any {
  //this.nav.classList.remove("white-bg");
}

}
