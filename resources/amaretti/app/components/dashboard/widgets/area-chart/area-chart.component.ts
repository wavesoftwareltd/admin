import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { PageSharedService } from '@resources/admin/app/services/page.shared.service';
import { Helper } from "@resources/core/helper";
import { ColorsHelper } from "@resources/amaretti/app/utils/colors";
import { TranslateService } from '@resources/core/services/translate.service';

declare var jQuery: any;
declare var App: any;
declare var $: any;
declare var tinycolor: any;
declare var Morris: any;
declare var numeral: any;

@Component({
    selector: 'dashboard-widget-area-chart',
    templateUrl: 'area-chart.template.html'
})
export class AreaChartComponent implements OnDestroy, OnInit {

    public id;
    public legendData;
    protected colorsHelper;

    @Input() chartData: any = [];
    @Input() title: any;
    @Input() isInit = true;
    @Input() colorsNumber = 0;
    @Input() colors = {};

    public constructor(
        private _sharedService: PageSharedService,
        private translator: TranslateService,
    ) {
        console.log('-------------------->');
    }

    public ngOnInit(): any {
        this.id = Helper.getUniqueString();
        this.colorsHelper = new ColorsHelper(this.colorsNumber);
        this.initWidget();
        //this.nav.className += " white-bg";
    }


    public ngOnDestroy(): any {
        //this.nav.classList.remove("white-bg"); 
    }

    public getFormatValue(info) {
        if (typeof info.is_currency !== 'undefined') {
            return Helper.convertToCurrency(info.data);
        } else {
            return numeral(info.data).format('0,0');
        }
    }

    public initWidget() {

        /*
        var data = [
            { label: "Premium Purchases", data: 15 },
            { label: "Standard Plans", data: 25 },
            { label: "Services", data: 60 }
        ];
        */
        var widgetId = this.id;
        var that = this;
        //  var colors = this.colorsHelper.getListOfColors(this.pieData.length);
        // this.legendData = this.colorsHelper.addColorsToList(this.pieData);
        /* var color1 = tinycolor(App.color.primary).lighten(5).toString();
        var color2 = App.color.alt2;
        var color3 = App.color.alt1;
*/
        console.log('widgetId', widgetId);
        $("#" + widgetId).ready(() => {
            console.log('widgetId', widgetId, 'ready');

            var color1 = App.color.alt2;
            var color2 = App.color.alt4;
            var color3 = App.color.alt3;
            var color4 = App.color.alt1;
            var color5 = tinycolor(App.color.primary).lighten(5).toString();
            var color6 = tinycolor(App.color.primary).lighten(5).toString();
            console.log(this.chartData);
            var formatData = [];
            var keys = {};
            for (var day in this.chartData) {
                var currnetData = this.chartData[day];
                for (var k in currnetData) {
                    keys[k] = true;
                }
                currnetData['day'] = day;
                formatData.push(currnetData);
            }
            var keysArray = [];
            var labelsArray = [];
            var lineColors = [];
            for (var k in keys) {
                keysArray.push(k);
                labelsArray.push(this.translator.__(k));
                if(typeof this.colors[k] !== 'undefined'){
                    lineColors.push(this.colors[k]);
                }else{ 
                }
            }
            if(lineColors.length < 1){
                lineColors = this.colorsHelper.getListOfColors(keysArray.length);
            }
            for(var i = 0;i < formatData.length;i++){
                for(var x = 0;x < keysArray.length;x++){
                    if(typeof formatData[i][keysArray[x]] === 'undefined' ){
                        formatData[i][keysArray[x]] = 0;
                    }
                }
            }
           
            for(var k in this.colors){

            }
            Morris.Area({
                element: widgetId,
                data: formatData,
                xkey: 'day',
                ykeys: keysArray,
                labels: labelsArray,
                behaveLikeLine: true,
                lineColors: lineColors,
                pointSize: 2,
                //fillOpacity : 0.6,
                hideHover: 'auto',
                dateFormat : (date) => {
                    return new Date(date).toLocaleDateString("he-IL");
                }
            });
        });


    }

}
