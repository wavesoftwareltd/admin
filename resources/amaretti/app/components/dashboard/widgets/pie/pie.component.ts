import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { PageSharedService } from '@resources/admin/app/services/page.shared.service';
import { Helper } from "@resources/core/helper";
import {ColorsHelper} from "@resources/amaretti/app/utils/colors";


declare var jQuery: any;
declare var App: any;
declare var $: any;
declare var tinycolor: any;
declare var numeral: any;

@Component({
    selector: 'dashboard-widget-pie',
    templateUrl: 'pie.template.html'
})
export class PieComponent implements OnDestroy, OnInit {

    public id;
    public legendData;
    protected colorsHelper;

    @Input() moreInfo: any;
    @Input() pieData: any;
    @Input() title: any;
    @Input() isShowLegendValues = true;
    @Input() radius = 0;
    @Input() colorsNumber = 0; 

    public constructor(
        private _sharedService: PageSharedService 
    ) {
    }

    public ngOnInit(): any {
        this.id = Helper.getUniqueString();
        this.colorsHelper = new ColorsHelper(this.colorsNumber);
        this.initWidget();
        //this.nav.className += " white-bg";
    }


    public ngOnDestroy(): any {
        //this.nav.classList.remove("white-bg"); 
    }

    public getFormatValue(info) {
        if(typeof info.is_currency !== 'undefined'){
            return Helper.convertToCurrency(info.data);
        }else{
            return numeral(info.data).format('0,0');
        }
    }

    public initWidget() {

        /*
        var data = [
            { label: "Premium Purchases", data: 15 },
            { label: "Standard Plans", data: 25 },
            { label: "Services", data: 60 }
        ];
        */
        var widgetId = this.id;
        var that = this;
        var colors = this.colorsHelper.getListOfColors(this.pieData.length);
        this.legendData = this.colorsHelper.addColorsToList(this.pieData);
        /* var color1 = tinycolor(App.color.primary).lighten(5).toString();
        var color2 = App.color.alt2;
        var color3 = App.color.alt1;
*/
        $("#"+widgetId).ready(() => {
            var legendContainer = $("#"+widgetId).closest('.widget').find(".legend");
            console.log(legendContainer);
                    $.plot('#'+widgetId, that.pieData, {
                        series: {
                            pie: {
                                show: true,
                                innerRadius: that.radius,
                                highlight: {
                                    opacity: 0.1
                                }
                            }
                        },
                        grid: {
                            hoverable: true
                        },
                        colors: colors
                    });
        });
        

    }

}
