import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import { FormsModule } from '@angular/forms';

import {BlankPageComponent} from "@resources/amaretti/app/components/pages/blank/blank.component";
import {BlankPageComponentV2} from "@resources/amaretti/app/components/pages/blank_v2/blank.component";
import {BasicPageComponent} from "@resources/amaretti/app/components/pages/basic/basic.component";


@NgModule({
  declarations: [
    BlankPageComponent,
    BasicPageComponent,
    BlankPageComponentV2
  ],
  imports: [
    FormsModule, 
    BrowserModule,
    RouterModule
  ],
  exports: [
    BlankPageComponent,
    BasicPageComponent,
    BlankPageComponentV2
  ],
})

export class AmarettiBasicLayoutModule {
}
 