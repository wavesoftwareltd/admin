import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@resources/core/modules/translate.module'; 

import {PieComponent} from "@resources/amaretti/app/components/dashboard/widgets/pie/pie.component";
import {AreaChartComponent} from "@resources/amaretti/app/components/dashboard/widgets/area-chart/area-chart.component";


@NgModule({
  declarations: [
    PieComponent,
    AreaChartComponent
  ],
  imports: [
    FormsModule, 
    BrowserModule,
    RouterModule,
    TranslateModule
  ],
  exports: [
    PieComponent,
    AreaChartComponent
  ],
})

export class AmarettiDashboardWidgetsModule {
}
 