import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import { FormsModule } from '@angular/forms';

import {AmarettiLoginComponent} from "@resources/amaretti/app/components/login/login.component";
import {AmarettiRegisterComponent} from "@resources/amaretti/app/components/register/register.component";
import {ValidationModule} from "@resources/core/modules/validation.module";
import { TranslateModule } from '@resources/core/modules/translate.module'; 

@NgModule({
  declarations: [
    AmarettiLoginComponent,
    AmarettiRegisterComponent
  ],
  imports: [
    FormsModule, 
    BrowserModule,
    ValidationModule,
    TranslateModule
  ],
  exports: [
    AmarettiLoginComponent,
    AmarettiRegisterComponent
  ],
})
export class AmarettiAdminUserModule {
}
 