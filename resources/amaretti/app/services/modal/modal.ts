
import { Helper } from "@resources/core/helper";
import { TranslateService } from "@resources/core/services/translate.service";
import { Injectable } from '@angular/core';
import { AviaryService } from '@resources/core/services/aviary.service'; 
import { WebService } from '@resources/core/services/web.service';
import { UploadService } from '@resources/core/services/upload.service';

declare var jQuery: any;

@Injectable()
export class ModalService {
    protected template = `
    <div id="[[id]]" class="modal-container modal-colored-header modal-colored-header modal-effect-10">
    <div class="modal-content">
            <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">[[title]]</h3>
            </div>
            <div class="modal-body">
                    <div class="text-center">
                            <div class="i-circle text-error"><i class="icon s7-check"></i></div>
                            <div class="main_content_wrap">
                                [[content]]
                            </div>
                    </div>
            </div> 
            <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default modal-close">[[cancelLabel]]</button>
                    <button type="button" data-dismiss="modal" class="btn btn-primary modal-ok">[[okLabel]]</button>
            </div>
    </div>
</div>
    `;

    protected alertTemplate = `
    <div id="[[id]]" class="modal-container modal-colored-header modal-colored-header modal-effect-10 modal-container-alert">
    <div class="modal-content">
            <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><i class="icon s7-close"></i></button>
                    <h3 class="modal-title">[[title]]</h3>
            </div>
            <div class="modal-body">
                    <div class="text-center">
                            <div class="main_content_wrap">
                                [[content]]
                            </div>
                    </div>
            </div> 
            <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-primary modal-ok">[[okLabel]]</button>
            </div>
    </div>
</div>
    `;


    protected customTemplate = `<div id="[[id]]" class="modal-container modal-colored-header custom-width modal-effect-9">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><i class="icon s7-close"></i></button>
        <h3 class="modal-title">[[title]]</h3>
      </div>
      <form id="[[id-form]]" method="post">
      <div class="modal-body form">
      <div class="main_content_wrap">
        [[content]]
       </div>
       <br>
      [[form]]
      </div>
      <div class="modal-footer">
      <button type="button" data-dismiss="modal" class="btn btn-default modal-close">[[cancelLabel]]</button>
      <button type="submit" data-dismiss="modal" class="btn btn-primary modal-ok">[[okLabel]]</button>
      </div>
      </form>
    </div>
  </div>`;

    protected id;
    protected title;
    protected content;
    protected formHtml;
    protected successCallback = (formData) => { };
    protected cancelCallback = () => { };
    protected okLabel = 'Confirm';
    protected cancelLabel = 'Cancel';

    constructor(
        private translateService: TranslateService,  
        private aviaryService : AviaryService,
        private webService: WebService,
        public uploadService: UploadService 
    ) {
        this.init();
    }

    /**
     * Create modal confirm box
     * @param title 
     * @param text 
     * @param successCallback 
     * @param cancelCallback 
     */
    alert(title, text, successCallback = (formData) => { }) {
        this.init();
        console.log('alert ***');
        var uniqueId = this.id;
        console.log(uniqueId);
        var that = this;
        this.setTitle(title);
        this.setContent(text);
        this.setSuccessCallback(successCallback);
        jQuery('body').append(this.getAlertRenderdHtml());
        jQuery('#' + uniqueId).niftyModal();
        jQuery('#' + uniqueId).addClass('modal-show');

        jQuery('#' + uniqueId + ' .modal-ok').click(() => {
            that.successCallback(true);
            jQuery('#' + uniqueId).fadeOut().remove();
        });

        jQuery('#' + uniqueId + ' .modal-close').click(() => {
            that.cancelCallback();
            jQuery('#' + uniqueId).fadeOut().remove();
        });


    }


    /**
     * Create modal confirm box
     * @param title 
     * @param text 
     * @param successCallback  
     * @param cancelCallback 
     */
    confirm(title, text, successCallback = (formData) => { }, cancelCallback = () => { }) {
        this.init();
        var uniqueId = this.id;
        console.log('confirm ***');
        console.log(uniqueId);
        var that = this;
        this.setTitle(title);
        this.setContent(text);
        this.setSuccessCallback(successCallback);
        this.setCancelCallback(cancelCallback);
        jQuery('body').append(this.getRenderdHtml());
        jQuery('#' + uniqueId).niftyModal();
        jQuery('#' + uniqueId).addClass('modal-show');

        jQuery('#' + uniqueId + ' .modal-ok').click(() => {
            that.successCallback(true);
            jQuery('#' + uniqueId).fadeOut().remove();
        });

        jQuery('#' + uniqueId + ' .modal-close').click(() => {
            that.cancelCallback();
            jQuery('#' + uniqueId).fadeOut().remove();
        });


    }


    /**
     * Create custom modal form box
     * @param title 
     * @param form - example
     * [
            {'title' : 'Name', 'content' : [
                {'type' : 'input',
                'name' : 'name',
                'placeholder' : 'Enter Name'}
            ]},
            {'title' : 'First Name', 'content' : [
                {'type' : 'input',
                'name' : 'first_name',
                'placeholder' : 'Enter First Name'},
                {'type' : 'input',
                'name' : 'last_name',
                'placeholder' : 'Enter Last Name'}
            ]},
            {'title' : 'Select test', 'content' : [
                {'type' : 'input',
                'name' : 'select',
                'placeholder' : 'Enter First Name',
                'options' : [
                    {'title' : 'some title','value': '1'},
                    {'title' : 'some title 2','value': '2'},
                    {'title' : 'some title 3','value': '3'},
                ]},
                {'type' : 'input',
                'name' : 'last_name',
                'placeholder' : 'Enter Last Name'}
            ]}
        ]
     * @param successCallback 
     * @param cancelCallback 
     */
    custom(title,text, form, successCallback = (formData) => { }, cancelCallback = () => { }) {
        this.init();
        var uniqueId = this.id;
        console.log('custom ***');
        console.log(uniqueId);
        console.log(form);
        var that = this;
        this.setTitle(title);
        this.setContent(text);
        this.buildStringFromForm(form);
        this.setSuccessCallback(successCallback);
        this.setCancelCallback(cancelCallback);
        jQuery('body').append(this.getCustomRenderdHtml());
        jQuery('#' + uniqueId).niftyModal();
        jQuery('#' + uniqueId).addClass('modal-show');
        jQuery('#' + uniqueId + '-form').validate(); 
        console.log(jQuery('#' + uniqueId + '-form').validate());
        console.log(jQuery('#' + uniqueId + '-form').validate().errorList.length);
        jQuery('#' + uniqueId + '-form').submit((e) => {
            e.preventDefault();
            console.log(jQuery('#' + uniqueId + '-form').validate());
            console.log(jQuery('#' + uniqueId + '-form').validate().errorList.length);
            if (jQuery('#' + uniqueId + '-form').validate().errorList.length < 1 && that.checkRequiredEmptyFields('#' + uniqueId + '-form')) {
                var formData = Helper.getFormData('#' + uniqueId + '-form');
                that.successCallback(formData);
                jQuery('#' + uniqueId).fadeOut().remove();
            }else{
                alert(that.translateService.__('Please fill all required fields'));
            }
        });

        jQuery('#' + uniqueId + ' .modal-close').click(() => {
            that.cancelCallback();
            jQuery('#' + uniqueId).fadeOut().remove();
        });


    }

    checkRequiredEmptyFields(targetForm){
       var res = true;
        jQuery(targetForm).find('[required]').each((index,elem) => {
            console.log(jQuery(elem)[0]);
          if(jQuery(elem).val().trim() == ''){
            res = false;
          } 
       });
      return res;
    }


    /**
     * Init modal
     */
    protected init() {
        this.id = Helper.getUniqueString();
        this.okLabel = this.translateService.__(this.okLabel);
        this.cancelLabel = this.translateService.__(this.cancelLabel);
    }

    /**
     * Get renderd html
     */
    protected getRenderdHtml() {
        var uniqueId = this.id;
        return this.template.replace('[[id]]', uniqueId)
            .replace('[[okLabel]]', this.okLabel)
            .replace('[[title]]', this.getTitle())
            .replace('[[content]]', this.getContent())
            .replace('[[cancelLabel]]', this.cancelLabel);
    }

    /**
     * Get renderd html
     */
    protected getCustomRenderdHtml() {
        console.log('this.id = ' + this.id);
        return this.customTemplate.replace('[[id]]', this.id)
            .replace('[[id-form]]', this.id + '-form')
            .replace('[[okLabel]]', this.okLabel)
            .replace('[[title]]', this.getTitle())
            .replace('[[form]]', this.formHtml)
            .replace('[[content]]', this.getContent())
            .replace('[[cancelLabel]]', this.cancelLabel);
    }

    /**
     * Get renderd html
     */
    protected getAlertRenderdHtml() {
        console.log('this.id = ' + this.id);
        return this.alertTemplate.replace('[[id]]', this.id)
            .replace('[[okLabel]]', this.okLabel)
            .replace('[[title]]', this.getTitle())
            .replace('[[content]]', this.getContent());
    }

    /**
     * Set title
     * @param string title 
     */
    public setTitle(title: string) {
        this.title = title;
    }

    /**
     * Set content
     * @param string content 
     */
    public setContent(content: string) {
        this.content = content;
    }

    /**
     * Build string from form json
     * @param array form 
     * example
     * [
            {'title' : 'Name', 'content' : [
                {'type' : 'input',
                'name' : 'name',
                'placeholder' : 'Enter Name'}
            ]},
            {'title' : 'First Name', 'content' : [
                {'type' : 'input',
                'name' : 'first_name',
                'placeholder' : 'Enter First Name'},
                {'type' : 'input',
                'name' : 'last_name',
                'placeholder' : 'Enter Last Name'}
            ]},
            {'title' : 'Select test', 'content' : [
                {'type' : 'input',
                'name' : 'select',
                'placeholder' : 'Enter First Name',
                'options' : [
                    {'title' : 'some title','value': '1'},
                    {'title' : 'some title 2','value': '2'},
                    {'title' : 'some title 3','value': '3'},
                ]},
                {'type' : 'input',
                'name' : 'last_name',
                'placeholder' : 'Enter Last Name'}
            ]}
        ]
     */
    public buildStringFromForm(form) {
        this.formHtml = '';
        var that = this;
        form.forEach(function (row) {
            let title = row.title;
            if (typeof row.title === 'string') {
                title = that.translateService.__(row.title);
            }
            that.formHtml += `
            <div class="form-group">
            <label>` + title + `</label>
            <div class="form-row">`;
            row.content.forEach(function (element) {
                var colNumber = 12 / row.content.length;
                that.formHtml += `<div class='` + ' col-md-' + colNumber + `'>`;
                element.class = (element.class ? element.class : '') + ' form-control ';

                switch (element.type) {
                    case 'text':
                    case 'textarea':
                        that.formHtml += `<textarea  `;
                        for (var key in element) {
                            // skip loop if the property is from prototype
                            if (!element.hasOwnProperty(key) || key == 'type') continue;
                            if (key == 'placeholder') {
                                var val = that.translateService.__(element[key]);
                            } else {
                                var val = element[key];
                            }

                            that.formHtml += key + `='` + val + `' `;
                        }
                        that.formHtml += `></textarea>`;
                        break;

                        case 'file':
                        case 'image':
                        var uniqueId = Helper.getUniqueString();
                        that.formHtml += `<div class="upload-image-box" data-modal-file-click="`+uniqueId+`">
                        <i class="s7-cloud-upload icon " data-modal-upload-icon="`+uniqueId+`" ></i>
                        <span data-modal-text-file-upload="`+uniqueId+`" >`+that.translateService.__(element['placeholder'] ? element['placeholder'] : 'Click here to upload file')+`</span>
                        </div>
                        <input type="text" style="display: none;" `+ (typeof element['required'] !== 'undefined' ? 'required="true"' : '')+`' name="`+element['name']+`" data-modal-file-input-name="`+uniqueId+`" />
                        <input type='file' style="display: none;" class="di_main_image_preview-input" data-modal-file-input="`+uniqueId+`" />`;
                       
                        jQuery('body').on('click', '[data-modal-file-click="'+uniqueId+'"]', function() {

                            
                          jQuery('[data-modal-file-input="'+uniqueId+'"]').change((event) => {
                            jQuery('[data-modal-file-click="'+uniqueId+'"]').addClass('pending');
                            jQuery('[data-modal-upload-icon="'+uniqueId+'"]').attr('class','fa fa-spinner fa-spin fa-3x fa-fw icon');
                            jQuery(`[data-modal-text-file-upload="`+uniqueId+`"]`).html(that.translateService.__('Uploading'));
                            jQuery('[data-modal-file-input-name="'+uniqueId+'"]').prop('required',true);
                           // console.log(event);
                          if (event.target.files && event.target.files[0]) {
                              var reader = new FileReader();
                              reader.onload = (event:any) => {
                                var res = event.target.result;
                                that.uploadService.uploadFileInput('[data-modal-file-input="'+uniqueId+'"]',element.type).then((res:any) => {
                                  //  console.log('vvvvvvvvvvvvvvv 1');
                                   // console.log(res);
                                    jQuery('[data-modal-upload-icon="'+uniqueId+'"]').attr('class','fa fa-check icon');
                                    jQuery('[data-modal-file-click="'+uniqueId+'"]').removeClass('pending').addClass('success');
                                    jQuery(`[data-modal-text-file-upload="`+uniqueId+`"]`).html(that.translateService.__('Upload Finished'));
                                    jQuery('[data-modal-file-input-name="'+uniqueId+'"]').val(res.url);
                                }).catch((ress) => { 
                                 //   console.log(ress);
                                    jQuery('[data-modal-file-click="'+uniqueId+'"]').removeClass('pending').addClass('error');
                                    jQuery(`[data-modal-text-file-upload="`+uniqueId+`"]`).html(that.translateService.__('Upload Error'));
                                });
                                
                              //  console.log(event.target.result); 
                              }
                          
                              reader.readAsDataURL(event.target.files[0]);
                            }
                        
                        });

                            //do something
                            console.log('-------------here------------');
                            jQuery('[data-modal-file-input="'+uniqueId+'"]').focus().trigger('click');


                          });
                        break;

                    case 'select':
                        that.formHtml += `<select  `;
                        for (var key in element) {
                            // skip loop if the property is from prototype
                            if (!element.hasOwnProperty(key) || key == 'type') continue;

                            if (key !== 'options' && key !== 'value') {
                                if (key == 'placeholder') {
                                    var val = that.translateService.__(element[key]);
                                } else {
                                    var val = element[key];
                                }
                                that.formHtml += key + `='` + val + `' `;
                            }
                        }
                        that.formHtml += `>`;
                        element.options.forEach(option => {
                            var selectedString = '';
                            let label = option.title;
                            if (typeof option.title === 'string') {
                                label = that.translateService.__(option.title);
                            }
                            if(element['value']){
                                if(element['value'] == option.value){
                                    selectedString = 'selected';
                                }
                            }else if(option.selected){
                                selectedString = 'selected';
                            }
                            that.formHtml += `<option `+selectedString+` value='` + option.value + `'>` + label + `</option>`;
                        });
                        that.formHtml += `></select>`;
                        break;

                    default:
                        that.formHtml += `<input  `;
                        for (var key in element) {
                            // skip loop if the property is from prototype
                            if (!element.hasOwnProperty(key)) continue;
                            if (key == 'placeholder') {
                                var val = that.translateService.__(element[key]);
                            } else {
                                var val = element[key];
                            }

                            that.formHtml += key + `='` + val + `' `;
                        }
                        that.formHtml += ` />`;
                        break;
                }
                that.formHtml += '</div>';
            });
            that.formHtml += `</div>
            </div>`;
        });
    }

    /**
     * Set success callback
     * @param successCallback 
     */
    public setSuccessCallback(successCallback) {
        this.successCallback = successCallback;
    }

    /**
     * Set cancel callback
     * @param cancelCallback 
     */
    public setCancelCallback(cancelCallback) {
        this.cancelCallback = cancelCallback;
    }

    /**
     * Get id
     */
    public getId() {
        return this.id;
    }

    /**
     * Get title
     */
    public getTitle() {
        var source = this.title;
        if(this.title.includes('-')){
         var arr = source.split('-');
         source = this.translateService.__(arr[0].trim())+'-'+arr[1];
        }
        return this.translateService.__(this.title);
    }

    /**
     * Get content
     */
    public getContent() {
        return this.translateService.__(this.content);
    }

    /**
     * Get success callback
     */
    public getSuccessCallback() {
        return this.successCallback;
    }

    /**
     * Get cancel callback
     */
    public getCancelCallback() {
        return this.cancelCallback;
    }



}
