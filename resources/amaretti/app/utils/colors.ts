
import { Helper } from "@resources/core/helper";

declare var jQuery: any;
declare var App: any;
declare var $: any;
declare var tinycolor: any;

/**
 * Colors helper
 */
export class ColorsHelper {
    
    protected colors = [];

    public constructor(
        protected steps = 1
    ) {
    }

    /**
     * Add colors to list array
     */
    public addColorsToList(list){
        var colors = this.getListOfColors(list.length);
        list.forEach(function(element,index) {
            list[index].color = colors[index];
        });
        return list;
    }

    public hexToRgb(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }
    

    /**
     * Get colors list array
     */
    public getColors(){
        if(this.colors.length < 1){
            this.colors = this.getDefaultColors();
        }
        return this.colors;
    }

    /**
     * Get default colors
     */
    public getDefaultColors(){
        var app = App.color.primary ? App.color : this.getDefaultAppColors();
        var colors = [
           tinycolor(app.primary).lighten(5).toString(),
           app.alt2,
           app.alt1,
           app.alt3,
           app.alt4,
            tinycolor(app.danger).lighten(5).toString(),
            tinycolor(app.info).lighten(5).toString(),
            tinycolor(app.success).lighten(5).toString(),
            tinycolor(app.warning).lighten(5).toString(),
            'rgb(83, 187, 131)',
            'rgb(245, 169, 47)',
            'rgb(245, 242, 47)',
            'rgb(145, 47, 245)',
        ];
        return Helper.moveArrayElemnts(colors,this.steps);
    }

    public getDefaultAppColors(){
        return {
            "primary":"rgb(239, 98, 98)",
            "success":"rgb(122, 204, 190)",
            "info":"rgb(141, 202, 223)",
            "warning":"rgb(255, 200, 112)",
            "danger":"rgb(239, 98, 98)",
            "alt1":"rgb(149, 217, 240)",
            "alt2":"rgb(255, 220, 122)",
            "alt3":"rgb(122, 152, 191)",
            "alt4":"rgb(204, 204, 204)"
        };
    }

    public getListOfColors(number,isHex = true){
        var result = []; 
        var currentLighten = 15;
        var colors = this.getColors();
        var colorsLength = colors.length;
        var currentStage = 0;
        for(var i = 0;i < number;i++){
            var color;
            if(typeof colors[i] !== 'undefined'){
                if(isHex){
                color = colors[i];
            }else{
                color = this.hexToRgb(colors[i]);
            }
                result.push(color);
            }else{
                color = tinycolor(colors[currentStage]).lighten(currentLighten).toString();
                if(!isHex){
                    color = this.hexToRgb(colors[i]);
                }
                result.push(color);
                currentStage++;
                if(currentStage >= colorsLength){
                    currentStage = 0;
                    currentLighten *= 2;
                }
            }
        }
        return result;
    }
    }
