import { Routes } from "@angular/router";

import { Dashboard1Component } from "./views/dashboards/dashboard1.component";
import { Dashboard2Component } from "./views/dashboards/dashboard2.component";
import { Dashboard3Component } from "./views/dashboards/dashboard3.component";
import { Dashboard4Component } from "./views/dashboards/dashboard4.component";
import { Dashboard41Component } from "./views/dashboards/dashboard41.component";
import { Dashboard5Component } from "./views/dashboards/dashboard5.component";

import { StarterViewComponent } from "./views/appviews/starterview.component";
import { LoginComponent } from "@resources/core/components/login/login.component";
import { RegisterComponent } from "@resources/core/components/register/register.component";

import { BlankLayoutComponent } from "./components/common/layouts/blankLayout.component"; 
import { BasicLayoutComponent } from "./components/common/layouts/basicLayout.component";
import { TopNavigationLayoutComponent } from "./components/common/layouts/topNavigationlayout.component";
import { AuthGuard } from '@resources/core/guards/auth.g';
import { LoginGuard } from '@resources/core/guards/login.g';

export const ROUTES: Routes = [
  // Main redirect
  { path: '', redirectTo: 'starterview', pathMatch: 'full' },

  
  // App views
  {
    path: 'dashboards', component: BasicLayoutComponent , canActivate: [AuthGuard] ,
    children: [
      { path: 'dashboard1', component: Dashboard1Component },
      { path: 'dashboard2', component: Dashboard2Component },
      { path: 'dashboard3', component: Dashboard3Component },
      { path: 'dashboard4', component: Dashboard4Component },
      { path: 'dashboard5', component: Dashboard5Component }
    ]
  },
  {
    path: 'dashboards', component: TopNavigationLayoutComponent , canActivate: [AuthGuard],
    children: [
      { path: 'dashboard41', component: Dashboard41Component }
    ]
  },
  {
    path: '', component: BasicLayoutComponent , canActivate: [AuthGuard],
    children: [
      { path: 'starterview', component: StarterViewComponent }
    ]
  },
  {
    path: '', component: BlankLayoutComponent,
    children: [
      { path: 'login', component: LoginComponent , canActivate: [LoginGuard] },
      { path: 'register', component: RegisterComponent },
    ]
  },

  // Handle all other routes
  { path: '**', redirectTo: 'starterview' }
];
