
// Imports
import { CKEditorComponent } from 'ng2-ckeditor';
import {
    Component,
    Input,
    Output,
    ViewChild,
    EventEmitter,
    NgZone,
    forwardRef,
    QueryList,
    AfterViewInit,
    ContentChildren,
    SimpleChanges,
    OnChanges
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { CKButtonDirective } from 'ng2-ckeditor';
import { CKGroupDirective } from 'ng2-ckeditor';
import { AuthService } from '@resources/core/services/auth.service';

declare var CKEDITOR: any;

/**
 * CKEditor component
 * Usage :
 *  <ckeditor [(ngModel)]="data" [config]="{...}" debounce="500"></ckeditor>
 */
@Component({
    selector: 'ckeditor',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AdminCKEditorComponent),
            multi: true
        },
    ],
    template: `<textarea #host></textarea>`
})
export class AdminCKEditorComponent extends CKEditorComponent {


    /**
     * On component destroy
     */
    ngOnDestroy() {
        if (this.instance) {
            setTimeout(() => {
                this.instance.removeAllListeners();
                // CKEDITOR.instances[this.instance.name].destroy();
                // this.instance.destroy();
                this.instance = null;
            });
        }
    }


    /**
     * CKEditor init
     */
    ckeditorInit(config: any) {
        var that = this;
        config.extraPlugins = 'uploadimage';
        config.uploadUrl = '/api/upload/image?token=' + AuthService.getToken();
        //config.filebrowserBrowseUrl = '/uploader/browse.php';
        config.filebrowserUploadUrl = '/api/upload/image-ckeditor?token=' + AuthService.getToken();
        super.ckeditorInit(config);
        this.instance.on('fileUploadResponse', function () {
            setTimeout(() => {
                that.instance.fire('change');
            }, 1);

        });
    }


}
