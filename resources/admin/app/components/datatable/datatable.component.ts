import { Component } from '@angular/core';
import { Directive, ElementRef } from '@angular/core';
import { Input, OnInit, AfterViewInit } from '@angular/core';
import { Helper } from "@resources/core/helper";
import { WebService } from "@resources/core/services/web.service";
import { TranslateService } from '@resources/core/services/translate.service';

declare var jQuery: any;

@Component({
  selector: 'jquery-datatable',
  templateUrl: 'datatable.template.html'
})
export class JqueryDatatableComponent implements AfterViewInit {

  private _datatable: any;
  public uniqueId: string = null;

  @Input() config: any;
  @Input() columns: any;


  constructor(private _element: ElementRef, private webService: WebService,
    private translator: TranslateService) { }

  ngAfterViewInit() {
    this.applyOptions();
  }

  applyOptions() {
    var that = this;
    if (!this.config) {
      console.error("Empty options array was passed to initialize datatable config.");
    }
    var config = this.config || {};


    config.language = this.getLangData();
    config.buttons =
      [
        'copyHtml5', 'csvHtml5', 'excelHtml5', 'pdfHtml5', 'print'
      ];
    // config.paging = false;

    var url = config.ajax;
    config.ajax = function (data, callback, settings) {
      that.webService.query(config.url, data, function (serverResult) {
        var result = {
          recordsFiltered: serverResult['recordsFiltered'],
          recordsTotal: serverResult['recordsTotal'],
          data: []
        };
        for (var x in serverResult.data) {
          var row = serverResult.data[x];
          var resultRow = [];
          that.columns.forEach(function (column) {
            if (typeof column['type'] !== 'undefined' && typeof row[column['key']] !== 'undefined') {
              var type = column['type'];
              var params = column['typeParams'] ? column['typeParams'] : {};
              var value = row[column['key']];
              resultRow.push(that.getHtmlByType(type, value, params, row));
            } else if (typeof column['function'] !== 'undefined' && typeof row[column['key']] !== 'undefined') {
              var columnFunction = column['function'];
              resultRow.push(columnFunction(row[column['key']], row));
            } else if (typeof row[column['key']] !== 'undefined') {
              resultRow.push(row[column['key']]);
            } else if (typeof column['value'] !== 'undefined') {
              if(typeof column['value'] === 'function'){
                var func = column['value'];
                resultRow.push(func(row));
              }else{
                resultRow.push(column['value']);
              }
            }
          });
          console.log(resultRow);
          result.data.push(resultRow);
        }
        // Pass the data to jQuery DataTables
        callback(result);

      });
    }


    config.dom = "<'row am-datatable-header'<'col-sm-6'l><'col-sm-6'f>>" +
      "<'row am-datatable-body'<'col-sm-12'tr>>" +
      "<'row am-datatable-footer'<'col-sm-5'i><'col-sm-7'p>>";

    config.searching = true;
    config.filter = true;

    this._datatable = jQuery('#' + this.getUniqueId()).DataTable(config);
    jQuery('#' + this.getUniqueId() + '_wrapper' + ' .buttons-print span').html(this.translator.__('Print'));
    jQuery('#' + this.getUniqueId() + '_wrapper' + ' .buttons-copy span').html(this.translator.__('Copy'));

  }

  getHtmlByType(type: string, value: string, params: any, row: any): string {
    var result = '';

    switch (type) {
      case 'link':
        let href = params['href'] ? params['href'] : '#';
        var clickFunciton = params['click'] ? params['click'] : function () { };
        var id = 'link-' + Helper.getUniqueString();

        for (let x in row) {
          let element = row[x];
          if (href.includes(':' + x)) {
            href = href.replace(':' + x, element);
          }
        }

        result = '<a id="' + id + '" class="auto-link" href="' + href + '">' + value + '</a>'; 

        jQuery('body').on('click', '#' + id, function (e) {
          clickFunciton(e, this,target);
        });

        break;

      case 'checkbox':
        let target = params['target'];
        var clickFunciton = params['click'] ? params['click'] : function () { };
        var changeFunciton = params['change'] ? params['change'] : function () { };
        let checkboxIndex = value;
        let isSelected = target[checkboxIndex] ? target[checkboxIndex] : false;
        target[checkboxIndex] = isSelected;
        var id = 'checkbox-' + Helper.getUniqueString();
        result = '<input type="checkbox" ' + (isSelected ? 'checked' : '') + ' name="' + id + '" id="' + id + '">';
        jQuery('body').on('change', '#' + id, function (e) {
          target[checkboxIndex] = jQuery(this).is(":checked");
          changeFunciton(e, this,target);
          clickFunciton(e, this,target);
        });

        break;
    }
    return result;
  }

  getLangData() {
    return {
      "decimal": "",
      "emptyTable": this.translator.__("No data available in table"),
      "info": this.translator.__("Showing") + " _START_ " + this.translator.__("to") + " _END_ " + this.translator.__("of") + " _TOTAL_ " + this.translator.__("entries"),
      "infoEmpty": this.translator.__("Showing 0 to 0 of 0 entries"),
      "infoFiltered": "(" + this.translator.__("filtered from") + " _MAX_ " + this.translator.__("total entries") + ")",
      "infoPostFix": "",
      "thousands": ",",
      "lengthMenu": this.translator.__("Show") + " _MENU_ " + this.translator.__("entries"),
      "loadingRecords": this.translator.__("Loading") + "...",
      "processing": this.translator.__("Processing") + "...",
      "search": this.translator.__("Search") + ":",
      "zeroRecords": this.translator.__("No matching records found"),
      "paginate": {
        "first": this.translator.__("First"),
        "last": this.translator.__("Last"),
        "next": this.translator.__("Next"),
        "previous": this.translator.__("Previous")
      },
      "aria": {
        "sortAscending": ": " + this.translator.__("activate to sort column ascending"),
        "sortDescending": ": " + this.translator.__("activate to sort column descending")
      }
    };
  }

  getUniqueId() {
    if (this.uniqueId == null) {
      this.uniqueId = Helper.getUniqueString();
    }
    return this.uniqueId;
  }

}
