import { Component } from '@angular/core';
import { Directive, ElementRef } from '@angular/core';
import { Input, OnInit, AfterViewInit } from '@angular/core';
import { Helper } from "@resources/core/helper";
import { WebService } from "@resources/core/services/web.service";
import { TranslateService } from '@resources/core/services/translate.service';

declare var jQuery: any;

@Component({
  selector: 'loader-bar',
  templateUrl: 'loader-bar.template.html'
})
export class LoaderBarComponent  {

  private _datatable: any;
  public uniqueId: string = null;

  @Input() text = 'Loading...';


  constructor(private _element: ElementRef, private webService: WebService,
    private translator: TranslateService) { }

}
 