import {NgModule} from "@angular/core";
import { CKEditorModule } from 'ng2-ckeditor';
import { CommonModule } from '@angular/common';

import {AdminCKEditorComponent} from "@resources/admin/app/components/ckeditor/ckeditor.component";
import { CKButtonDirective } from 'ng2-ckeditor';
import { CKGroupDirective } from 'ng2-ckeditor';

@NgModule({
  declarations: [
    AdminCKEditorComponent
  ],
  imports: [
    CommonModule,
    CKEditorModule
  ],
  exports: [
    AdminCKEditorComponent
  ],
})

export class CkeditorModule {
}
 