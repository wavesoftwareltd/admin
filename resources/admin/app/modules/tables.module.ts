import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import { FormsModule } from '@angular/forms';

import {JqueryDatatableComponent} from "@resources/admin/app/components/datatable/datatable.component";
import { TranslateModule } from '@resources/core/modules/translate.module'; 

@NgModule({
  declarations: [
    JqueryDatatableComponent
  ],
  imports: [
    FormsModule, 
    BrowserModule,
    RouterModule,
    TranslateModule
  ],
  exports: [
    JqueryDatatableComponent
  ],
})

export class TablesModule {
}
 