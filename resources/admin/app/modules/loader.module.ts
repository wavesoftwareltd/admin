import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import { FormsModule } from '@angular/forms';

import {LoaderBarComponent} from "@resources/admin/app/components/loaders/loader-bar/loader-bar.component.ts";
import { TranslateModule } from '@resources/core/modules/translate.module'; 

@NgModule({
  declarations: [
    LoaderBarComponent
  ],
  imports: [
    FormsModule, 
    BrowserModule,
    RouterModule,
    TranslateModule
  ],
  exports: [
    LoaderBarComponent
  ],
})

export class LoaderModule {
}
 