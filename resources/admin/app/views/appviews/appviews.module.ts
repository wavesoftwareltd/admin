import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import { FormsModule, NG_VALIDATORS } from '@angular/forms';

import {StarterViewComponent} from "./starterview.component";
import {ValidationModule} from "@resources/core/modules/validation.module";
import {AdminUserModule} from "@resources/core/modules/adminUser.module";

import {PeityModule } from '../../components/charts/peity';
import {SparklineModule } from '../../components/charts/sparkline';

@NgModule({
  declarations: [
    StarterViewComponent
  ],
  imports: [
    FormsModule, 
    BrowserModule,
    RouterModule,
    PeityModule,
    SparklineModule,
    ValidationModule,
    AdminUserModule
  ],
  exports: [
    StarterViewComponent,
    ValidationModule
  ],
})

export class AppviewsModule {
}
