<?php

/**
 * Arbel Admin Configuration
 */

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
use Arbel\Admin\Controller\MainController;
use Arbel\Service\Factory\DiServiceFactory;

return [
    'service_manager' => [
        'factories' => [
        ],
        'di' => [
            'allowed_controllers' => [
                // this config is required, otherwise the MVC won't even attempt to ask Di for the controller!
               MainController::class,
            ],
            'instance' => [
                'preference' => [
                ],
            ],
        ]
    ],
    'router' => [
        'routes' => [
            'admin' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/manager[/:action]',
                    'defaults' => [
                        'controller' => MainController::class,
                        'admin_prefix' => 'manager',
                        'action' => 'index',
                    ],
                ],
            ],
        ]
    ],
    'controllers' => [
        'factories' => [
            MainController::class => DiServiceFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'arbel/admin/layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'arbel/admin/main/index' => __DIR__ . '/../view/admin/main/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    
];
